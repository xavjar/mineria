# -- coding: utf-8 --
import scrapy
from scrapy.http import Request
from scrapy.selector import HtmlXPathSelector
from scrapy.spiders import CrawlSpider
# from scrapy import signals
# from scrapy.xlib.pydispatch import dispatcher
#import logging


class EvaSpider(CrawlSpider):
    name = 'evaspider'
    start_urls = ['https://eva3.utpl.edu.ec/login/index.php']
    allowed_domains = ['eva3.utpl.edu.ec']
    main_path = "https://eva3.utpl.edu.ec"
    table_name="data"
    comments_img=0
    comments_video=0
    comments_text=0
    firts_post=True

    # def __init__(self):
    #     dispatcher.connect(self.spider_close, signals.spider_closed)aa

    def closed(self, spider):
        print "}]"
        # print "Comentarios con imagenes: --- ",self.comments_img
        # print "Comentarios con videos:   --- ",self.comments_video
        # print "Comentarios solo texto:   --- ",self.comments_text
        # print "Total Comentarios:        --- ",(self.comments_img+self.comments_video+self.comments_text)

    def parse(self, response):
        return scrapy.FormRequest.from_response(
            response,
            formdata={'username': 'hxjaramillo', 'password': '56443'},
            callback=self.after_login
        )

    def after_login(self, response):        
        # check login succeed before going on
        if "authentication failed" in response.body:
            #self.logger.error("Login failed")
            return
        else:
            #return Request(url="https://eva3.utpl.edu.ec/mod/forum/view.php?id=924744", callback=self.pageCourses)
            return Request(url="https://eva3.utpl.edu.ec/my/index.php", callback=self.pageCourses)
    #Take courses.
    def pageCourses(self, response):
        sel = scrapy.Selector(response)
        courseList = sel.xpath('//div[contains(@class, "course_list")]//a')
        
        print "[{"
        #Course
        for course in courseList:
            course_url=self.main_path+course.xpath('@href').extract()[0]
            request = Request(url=course_url, callback=self.pageCourse)
            yield request 
        
             
    #Take foros
    def pageCourse(self, response):
        sel = scrapy.Selector(response)
        forumList=sel.css('.forum a')
        for forum in forumList:
            forum_url=self.main_path+forum.xpath('@href').extract()[0]
            request = Request(url=forum_url, callback=self.pageForum)
            yield request 

    #Take foro content
    def pageForum(self, response):
        sel = scrapy.Selector(response)
        postList=sel.css('.forumpost')

        for post in postList:
            title=post.xpath('.//div[contains(@class, "subject")]/text()').extract()[0].encode('utf8').replace("\n","").strip()
            author=post.xpath('.//div[contains(@class, "author")]/a/text()').extract()[0].encode('utf8').replace("\n","").strip()
            cont_post=post.css('.fullpost').xpath('string(.)').extract()[0].encode('utf8').strip().replace("\r\n"," ")
            img=post.css('.fullpost').xpath('.//img')
            date_post=post.css('.author').xpath('string(.)').extract()[0].encode('utf8').split('-')[1].strip()
            
            if img:#Check imgs
                self.comments_img+=1
            elif cont_post.find("https://")>0 or cont_post.find("http://")>0:#check videos
                self.comments_video+=1
            else:
                self.comments_text+=1 #Counting comments only text

            # print "insert into "+self.table_name+" values( '"\
            #     +str(title)\
            #     +"', '"+str(author)\
            #     +"', '"+str(cont_post)\
            #     +"', '"+str(date_post)\
            #     +"'); \n"
            if self.firts_post:
                self.firts_post=False
                print "\"title\":\""\
                    +str(title)\
                    +"\", \"author\": \""+str(author)\
                    +"\", \"message\":\""+str(cont_post)\
                    +"\", \"date_created\":\""+str(date_post)\
                    +"\""
            else:
                print "},{\"title\":\""\
                    +str(title)\
                    +"\", \"author\":\""+str(author)\
                    +"\", \"message\":\""+str(cont_post)\
                    +"\", \"date_created\":\""+str(date_post)\
                    +"\""
