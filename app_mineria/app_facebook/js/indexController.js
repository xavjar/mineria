/*

Author: flromero
Date: Sep-2017
*/
var app = angular.module('myApp', []).controller('indexController', ['$scope', '$http', function($scope, $http) {
    $scope.navs = {
        'nav1': false,
        'nav2': true,
        'nav3': true
    };
    $scope.fb = {};
    $scope.results = [];
    $scope.summ = {
        link: 0,
        photo: 0,
        video: 0,
        reply: 0,
        status: 0,
        comment: 0
    };
    $scope.search = function() {
        $http.post('process_json.php', $scope.fb).then(function(response) {
            $scope.results = response.data.items;
        }, function(failure) {
            console.log("failed :(", failure);
        });
    }
    $scope.tabs = function(nav) {
        switch (nav) {
            case 1:
                $scope.navs.nav1 = false;
                $scope.navs.nav2 = true;
                $scope.navs.nav3 = true;
                break;
            case 2:
                $scope.navs.nav1 = true;
                $scope.navs.nav2 = false;
                $scope.navs.nav3 = true;
                break;
            case 3:
                $scope.navs.nav1 = true;
                $scope.navs.nav2 = true;
                $scope.navs.nav3 = false;
                break;
        }
    }
    //<---- Eva
    $scope.eva = {};
    $scope.eva_items = [];
    $scope.eva_summ = {
        texto: 0,
        img: 0,
        video: 0
    };
    $scope.eva_search = function() {
        console.log("eva_search");
        $http.post('process_eva.php', $scope.eva).then(function(response) {
            $scope.eva_items = response.data.items;
        }, function(failure) {
            console.log("failed :(", failure);
        });
    } // Eva --->
}]);

app.filter('dateRange', function() {
    return function(items, fromDate, toDate, scope) {
        scope.summ.video = 0;
        scope.summ.link = 0;
        scope.summ.comment = 0;
        scope.summ.photo = 0;
        scope.summ.status = 0;
        var filtered = [];
        var from_date = new Date(fromDate);
        var to_date = new Date(toDate);
        angular.forEach(items, function(item) {
            if (fromDate || toDate) {
                var date_item = new Date(item.created_time);
                if (date_item >= from_date && date_item <= to_date) {
                    filtered.push(item);
                    switch (item.type) {
                        case "status":
                            scope.summ.status++;
                            break;
                        case "comment":
                            scope.summ.comment++;
                            break;
                        case "link":
                            scope.summ.link++;
                            break;
                        case "photo":
                            scope.summ.photo++;
                            break;
                        case "video":
                            scope.summ.video++;
                            break;
                    }
                }
            } else {
                switch (item.type) {
                    case "status":
                        scope.summ.status++;
                        break;
                    case "comment":
                        scope.summ.comment++;
                        break;
                    case "link":
                        scope.summ.link++;
                        break;
                    case "photo":
                        scope.summ.photo++;
                        break;
                    case "video":
                        scope.summ.video++;
                        break;
                }
            }
        });
        if (!fromDate || !toDate) {
            filtered = items;
        };
        return filtered;
    }
});



app.filter('eva_dateRange', function() {
    return function(items, fromDate, toDate, scope) {
        scope.eva_summ.texto = 0;
        scope.eva_summ.img = 0;
        scope.eva_summ.video = 0;
        var filtered = [];
        // var from_date = new Date(fromDate);
        // var to_date = new Date(toDate);
        var from_date = Date.parse(fromDate);
        var to_date = Date.parse(toDate);
        angular.forEach(items, function(item) {
            if (fromDate || toDate) {
                // var date_item = new Date(item.created_time);
                var date_item = Date.parse(item.created_time);
                if (date_item >= from_date && date_item <= to_date) {
                    filtered.push(item);
                    switch (item.type) {
                        case "texto":
                            scope.eva_summ.texto++;
                            break;
                        case "img":
                            scope.eva_summ.img++;
                            break;
                        case "video":
                            scope.eva_summ.video++;
                            break;
                    }
                }
            } else {
                 switch (item.type) {
                        case "texto":
                            scope.eva_summ.texto++;
                            break;
                        case "img":
                            scope.eva_summ.img++;
                            break;
                        case "video":
                            scope.eva_summ.video++;
                            break;
                    }
            }
        });
        if (!fromDate || !toDate) {
            filtered = items;
        };
        return filtered;
    }
});
