<?php
require __DIR__ . "/bin/vendor/autoload.php"; 
use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

$json = file_get_contents('php://input');
$_POST = json_decode($json, true);

$client = new Client();
$main_path = "https://eva3.utpl.edu.ec";
$resultados= array('items'=>[]);

// Ingresar
$crawler = $client->request('GET', 'https://eva3.utpl.edu.ec/login/ingresoManual.php');
$form = $crawler->selectButton('Ingresar')->form();
$crawler = $client->submit($form, array('username' => $_POST['user'], 'password' => $_POST['pass']));

//Pagina de lista de Cursos.
$crawler = $client->request('GET', 'https://eva3.utpl.edu.ec/my/index.php');

//Recorrer los cursos.
$courses=$crawler->filterXpath('//div[contains(@class, "course_list")]//a');

  //create items
  function getDateCustom($date_in) {
  	$aux=explode(', ', $date_in)[1];
  	$aux=explode(' de ', $aux);
  	$meses_w = array("enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre");
	$meses_n = array(1,2,3,4,5,6,7,8,9,10,11,12);

	$anio=$aux[2];
	$mes=str_pad(str_replace($meses_w,$meses_n,$aux[1]), 2, "0", STR_PAD_LEFT);
	$dia=str_pad($aux[0],2,"0", STR_PAD_LEFT);

  	$date_out=$anio."-".$mes."-".$dia;
  	return $date_out;
  }


foreach ($courses as $course) {
	$crawler = $client->request('GET', $main_path.$course->getAttribute('href'));
	$foros=$crawler->filter('.forum a');
	//Foros
	foreach ($foros as $foro) {
		$crawler = $client->request('GET', $main_path.$foro->getAttribute('href'));
		$posts=$crawler->filter('.forumpost');

		//#Take foro content
		foreach ($posts as $post) {
			$crawler1 = new Crawler($post);
			$title=$crawler1->filterXpath('//div[contains(@class, "subject")]')->text();
			$author=$crawler1->filterXpath('//div[contains(@class, "author")]/a')->text();
			$message=$crawler1->filter('.fullpost')->text();
			$created_time=explode("-",$crawler1->filter('.author')->text())[1];
			$created_time=getDateCustom($created_time);

			$img=$crawler1->filter('.fullpost img');
			if (count($img)>0) {
				$type="img";
			}elseif (strpos($message, 'https://') || strpos($message, 'http://')) {
				$type="video";
			}else{
				$type="texto";
			}

			//Guardar cada post
			$item= array(
			    'title' => $title,
				'message' => $message,
				'created_time' => $created_time,
				'type' => $type,
				'author' => $author
				);
			array_push($resultados['items'], $item);
		}
	}
}

$serializedData = serialize($resultados);

//$serializedData2 = serialize($resultados);

// save serialized data in a text file
//file_put_contents('datos.txt', $serializedData);

// at a later point, you can convert it back to array like:
//$recoveredData = file_get_contents('datos.txt');

// unserializing to get actual array
//$recoveredArray = unserialize($recoveredData);

echo json_encode($resultados);
file_put_contents('datos.json', json_encode($resultados));
// at a later point, you can convert it back to array like:
$recoveredData = file_get_contents('datos.json');

?>

