import os
from flask import Flask, render_template
app = Flask(__name__)

@app.route('/')
def index():
 return render_template('index.html')

@app.route('/my-link/')
def my_link():
  os.system('python3 1.py')
  print ('I got clicked!')

  return 'Click.'

if __name__ == '__main__':
  app.run(debug=True)
