
<?php
/*
Author: flromero
Date: Jan 2018
*/
header('Content-Type: application/json');
 require('bin/vendor/autoload.php');
 $json = file_get_contents('php://input'); 
 $obj = json_decode($json);


 $app_id=$obj->app_id;
 $app_secret=$obj->app_secret;
 $token=$obj->token;
 $group_id=$obj->group_id;
 
 $sql="";
 $html_result="";

 $results= array('items'=>[]);


 $fb = new Facebook\Facebook([
  'app_id' => $app_id,
  'app_secret' => $app_secret
  ]);

  	
  //create items
  function saveInfo($title, $message, $author,$created_time, $type) {
	global $results;
	$item= array('title' => $title, 
		'message'=>$message, 
		'created_time'=>$created_time, 
		'type'=>$type, 
		'author'=>$author);
	array_push($results['items'], $item);	
  }

try {
  // Get the \Facebook\GraphNodes\GraphUser object for the current user.
  // If you provided a 'default_access_token', the '{access-token}' is optional.
  $response = $fb->get($group_id.'?fields=feed{created_time,type, message,from,comments{created_time,message,from,comments{created_time,message,from}}}',$token);
} catch(\Facebook\Exceptions\FacebookResponseException $e) {
  // When Graph returns an error
  echo 'Graph returned an error: ' . $e->getMessage();
  exit;
} catch(\Facebook\Exceptions\FacebookSDKException $e) {
  // When validation fails or other local issues
  echo 'Facebook SDK returned an error: ' . $e->getMessage();
  exit;
}


$graphNode = $response->getGraphNode();
$feeds=$graphNode['feed'];


$nombre_archivo = "results/data".date("dmY_Hms").".sql"; 
//Loop posts
foreach ($feeds as $feed) {
	$message = isset($feed['message']) ? $feed['message'] : null;
	$type = isset($feed['type']) ? $feed['type'] : null;
	$created_time = isset($feed['created_time']) ? $feed['created_time']->format('Y-m-d') : null;
	$author = isset($feed['from']) ? $feed['from']['name'] : null;
	



	saveInfo($title,$message,$author,$created_time,$type);
	
	//Loop comments
	if(isset($feed['comments'])){
		$comments=$feed['comments'];
		foreach ($comments as $comment) {
			$comment_msg = isset($comment['message']) ? $comment['message'] : null;
			$comment_created_time = isset($comment['created_time']) ? $comment['created_time']->format('Y-m-d') : null;
			$comment_aut = isset($comment['from']) ? $comment['from']['name']: null;
			saveInfo($title,$comment_msg,$comment_aut,$comment_created_time,'comment');

			//Loop Replys
			if(isset($comment['comments'])){
				$comment_replys=$comment['comments'];
				foreach ($comment_replys as $reply) {
					$reply_msg = isset($reply['message']) ? $reply['message'] : null;
					$reply_created_time = isset($reply['created_time']) ? $reply['created_time']->format('Y-m-d') : null;
					$reply_aut = isset($reply['from']) ? $reply['from']['name']: null;
					saveInfo($title,$reply_msg,$reply_aut,$reply_created_time,'reply');
				}
			}					
		}
	} 	
}



$serializedData = serialize($results);
// save serialized data in a text file
///file_put_contents('datos_fb.txt', $serializedData);
// at a later point, you can convert it back to array like:
///$recoveredData = file_get_contents('datos_fb.txt');

echo json_encode($results);
file_put_contents('datos_fb.json', json_encode($results));
// at a later point, you can convert it back to array like:
$recoveredData = file_get_contents('datos_fb.json');
?>
