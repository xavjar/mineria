import spacy
from spacy import displacy
from spacy.lang.en.stop_words import STOP_WORDS
from pathlib import Path
from IPython.display import display, HTML

f= open('your_file_name.txt', 'r')
f1 = f.read()
print ("--------------------")
print (f1)
sample = u"I can't imagine spending $3000 for a single bedroom apartment in N.Y.C."

nlp = spacy.load('es')
doc = nlp(f1)

f.close()
for token in doc:
    print(token)


tokens = [token for token in doc]
print(tokens)

for word in doc:
    if word.is_stop == True:
        print(word)

for token in doc:
    print(token.text, token.lemma_, token.pos_, token.tag_, token.dep_,
          token.shape_, token.is_alpha, token.is_stop)


displacy.render(doc, style='dep', jupyter=True, options={'distance': 70})

for ent in doc.ents:
    print(ent.text, ent.start_char, ent.end_char, ent.label_)

displacy.render(doc, style='ent', jupyter=True)

svg = displacy.render(doc, style='dep')
output_path = Path('sentence.svg')
output_path.open('w', encoding='utf-8').write(svg)
