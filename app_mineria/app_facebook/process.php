<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
</head>
<body>
<div class="container">

<?php
/*
Author: flromero
Date: Sep 2017
*/
header('Content-Type: text/html; charset=utf-8'); 
 require('bin/vendor/autoload.php');
 //$token="168891293688258|jMghkVtuRY5u1P4Sdl0C8K4unUI";
 $app_id=$_POST['app_id'];
 $app_secret=$_POST['app_secret'];
 $token=$_POST['token'];
 $group_id=$_POST['group_id'];
 
 $sql="";
 $html_result="";


 $fb = new Facebook\Facebook([
  'app_id' => $app_id,
  'app_secret' => $app_secret
  ]);

  function createVisual($title, $message, $author) {
	global $html_result;
	$html_result.='<tr>';
	$html_result.='<td>'.$title.'</td>';
    	$html_result.='<td>'.$message.'</td>';
	$html_result.='<td>'.$author.'</td>';
	$html_result.='</tr>';
  }
  
  function saveInfo($title, $message, $author) {
	createVisual($title, $message, $author);
	global $sql;
	$message=str_ireplace("\n"," ",$message);
	if($title==null){
		$title="null";
	}
	
    $sql.="insert into data values (".$title.", ";
	$sql.="'".$message."', ";
	$sql.="'".$author."');\r\n";
	
  }

try {
  // Get the \Facebook\GraphNodes\GraphUser object for the current user.
  // If you provided a 'default_access_token', the '{access-token}' is optional.
  $response = $fb->get($group_id.'?fields=feed{message,from,comments{message,from,comments{message,from}}}',$token);
} catch(\Facebook\Exceptions\FacebookResponseException $e) {
  // When Graph returns an error
  echo 'Graph returned an error: ' . $e->getMessage();
  exit;
} catch(\Facebook\Exceptions\FacebookSDKException $e) {
  // When validation fails or other local issues
  echo 'Facebook SDK returned an error: ' . $e->getMessage();
  exit;
}


$graphNode = $response->getGraphNode();
$feeds=$graphNode['feed'];



$nombre_archivo = "results/data".date("dmY_Hms").".sql"; 
//Loop posts
foreach ($feeds as $feed) {
	if(isset($feed['message'])&&isset($feed['from']['name'])){
		$message=$feed['message'];
		$author=$feed['from']['name'];
	    saveInfo($title,$message,$author);
	} 
	//Loop comments
	if(isset($feed['comments'])){
		$comments=$feed['comments'];
		foreach ($comments as $comment) {
			if(isset($comment['message'])&&isset($comment['from']['name'])){
				$comment_msg=$comment['message'];
				$comment_aut=$comment['from']['name'];
				saveInfo($title,$comment_msg,$comment_aut);
				
				//Loop Replys
				if(isset($comment['comments'])){
					$comment_replys=$comment['comments'];
					foreach ($comment_replys as $reply) {
						if(isset($reply['message'])&&isset($reply['from']['name'])){
							$reply_msg=$reply['message'];
							$reply_aut=$reply['from']['name'];
							saveInfo($title,$reply_msg,$reply_aut);		
						}						
					}
				}
			}					
		}
	}	
}
//echo '>> '.$sql;

//Write file
$nombre_archivo = "results/data".date("dmY_Hms").".sql"; 
if($archivo = fopen($nombre_archivo, "a"))
    {
        if(fwrite($archivo, $sql."\n"))
        {
            echo "Se ha ejecutado correctamente";
			echo '<br><a href="'.$nombre_archivo.'" class="btn btn-info btn-sm">';
            echo '<span class="glyphicon glyphicon-download-alt"></span> Download Results </a>';
        }
        else
        {
            echo "Ha habido un problema al crear el archivo";
        }
 
        fclose($archivo);
    }
	

?>

<table class="table table-striped">
    <thead>
      <tr>
        <th>Title</th>
        <th>Message</th>
        <th>Author</th>
      </tr>
    </thead>
    <tbody>
      <?php
	  echo ''.$html_result;
	  ?>
	   </tbody>
  </table>


  
    
</div>

</body>
</html>
