import spacy
from spacy import displacy
from spacy.lang.en.stop_words import STOP_WORDS
from pathlib import Path
from IPython.display import display, HTML
import json
import re
import os

print ("INICIAMOS")
#with open("datos.json") as f:
#  raw_data = f.read()
#data = json.loads(raw_data.decode("utf-8-sig"))
#os.system('./test.sh')
f= open('datos.txt', 'r')
print ("-----+++-------")
f1 = f.read()

print ("--------------------")
#print (leer)
#sample = u"I can't imagine spending $3000 for a single bedroom apartment in N.Y.C."
i=0
for i in range (100):
	rep = {"message": "", "items": "", "title":"", "created_time":"", "type":"", "author":"", "a:5:{s:5:":"" ,";s:29:":"","Re: ":"", "texto":"","s:"+str(i):"","i:"+str(i):"",";":"",'"':'',"{":"","}":"","Sloodle29"+str(i):"","::":""} 


	rep = dict((re.escape(k), v) for k, v in rep.items())
	pattern = re.compile("|".join(rep.keys()))
	f1 = pattern.sub(lambda m: rep[re.escape(m.group(0))], f1)


nlp = spacy.load('es')
doc = nlp(f1)

f.close()
for token in doc:
    print(token)


tokens = [token for token in doc]
print(tokens)

for word in doc:
    if word.is_stop == True:
        print(word)

for token in doc:
    print(token.text, token.lemma_, token.pos_, token.tag_, token.dep_,
          token.shape_, token.is_alpha, token.is_stop)


displacy.render(doc, style='dep', jupyter=True, options={'distance': 70})

for ent in doc.ents:
    print(ent.text, ent.start_char, ent.end_char, ent.label_)

displacy.render(doc, style='ent', jupyter=True)

svg = displacy.render(doc, style='dep')
output_path = Path('sentence.svg')
output_path.open('w', encoding='utf-8').write(svg)


